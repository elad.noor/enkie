"""Test module for I/O of COBRA models."""

import pytest
from rpy2.robjects.packages import isinstalled

from enkie import CompartmentParameters, ParameterSpace
from enkie.io.cobra import get_parameters_for_reaction_subset


@pytest.mark.parametrize(
    "rid, expected_ec, expected_gene_id, expected_uniprot_ac, expected_param_number",
    [
        ("ENO", "4.2.1.11", "b2779", "P0A6P9", 9),
        ("PGI", "5.3.1.9", "b4025", "P0A6T1", 8),
    ],
)
def test_get_params(
    core_model,
    rid,
    expected_ec,
    expected_gene_id,
    expected_uniprot_ac,
    expected_param_number,
):
    """Test getting parameters from a COBRA model."""

    reactions, rate_laws, enzymes, metabolites = get_parameters_for_reaction_subset(
        core_model, reaction_ids=[rid]
    )

    assert len(enzymes) == 1
    assert enzymes[0].ec == expected_ec
    assert enzymes[0].gene_ids[0] == expected_gene_id
    assert enzymes[0].uniprot_acs[0] == expected_uniprot_ac

    compartment_parameters = CompartmentParameters.load("e_coli")

    if not isinstalled("brms"):
        pytest.skip("brms not installed")

    parameter_space = ParameterSpace(
        reactions, rate_laws, enzymes, metabolites, compartment_parameters
    )
    assert parameter_space.mean.shape[0] == expected_param_number
