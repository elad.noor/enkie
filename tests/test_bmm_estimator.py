from typing import Optional

import pytest
from pytest_lazy_fixtures import lf
from rpy2.robjects.packages import isinstalled

from enkie import Enzyme, MiriamMetabolite, ModularRateLaw
from enkie.estimators import BmmKineticEstimator, KineticParameterType


@pytest.fixture(scope="session")
def bmm_estimator() -> Optional[BmmKineticEstimator]:
    if not isinstalled("brms"):
        return None
    return BmmKineticEstimator()


@pytest.fixture()
def eno_query(
    eno: ModularRateLaw,
    P0A6P9: Enzyme,
    _2pg: MiriamMetabolite,
    pep: MiriamMetabolite,
):
    return (
        [eno] * 4,
        [P0A6P9] * 4,
        [
            KineticParameterType.K_CAT_FORWARD,
            KineticParameterType.K_CAT_BACKWARD,
            KineticParameterType.K_M,
            KineticParameterType.K_M,
        ],
        [None, None, _2pg, pep],
    )


@pytest.fixture()
def pgm_query(
    pgm: ModularRateLaw,
    P62707: Enzyme,
    _2pg: MiriamMetabolite,
    _3pg: MiriamMetabolite,
):
    return (
        [pgm] * 4,
        [P62707] * 4,
        [
            KineticParameterType.K_CAT_FORWARD,
            KineticParameterType.K_CAT_BACKWARD,
            KineticParameterType.K_M,
            KineticParameterType.K_M,
        ],
        [None, None, _2pg, _3pg],
    )


@pytest.mark.parametrize(
    "query",
    [
        lf("eno_query"),
        lf("pgm_query"),
    ],
)
def test_estimation(query, bmm_estimator: BmmKineticEstimator):
    """Verify that the BMM estimator returns the correct number of estimates."""

    # ensure we have the needed R packages
    if bmm_estimator is None:
        pytest.skip("R package `brms` not installed")

    ln_k_mean, ln_k_cov = bmm_estimator.get_parameters(*query)

    n_reactions = len(query[0])
    assert ln_k_mean.size == n_reactions
    assert ln_k_cov.shape == (n_reactions, n_reactions)
