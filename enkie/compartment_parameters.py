"""Parameters for the compartments of a biochemical network.
"""

from importlib import resources
from typing import Dict, TextIO

import pandas as pd

from .constants import DEFAULT_I, DEFAULT_PH, DEFAULT_PHI, DEFAULT_PMG, DEFAULT_T, Q
from .utils import get_path


COMPARTMENT_ANY = "__any__"
"""Pseudo-identifier representing any compartment.
"""


class CompartmentParameters:
    """Parameters for the compartments of a metabolic network.

    Parameters
    ----------
    compartment_pH : Dict[str, Q], optional
        Mapping from compartment identifiers to the pH of the compartment.
    compartment_pMg : Dict[str, Q], optional
        Mapping from compartment identifiers to the pMg of the compartment.
    compartment_I : Dict[str, Q], optional
        Mapping from compartment identifiers to the ionic strength of the compartment.
    compartment_phi : Dict[str, Q], optional
        Mapping from compartment identifiers to the electrostatic potential of the
        compartment.
    temperature : Q, optional
        Temperature of the system (temperature must be the same for all compartments).
    """

    RESOURCE_PACKAGE_NAME = "enkie.data.compartment_parameters"

    def __init__(
        self,
        compartment_pH: Dict[str, Q] = None,
        compartment_pMg: Dict[str, Q] = None,
        compartment_I: Dict[str, Q] = None,
        compartment_phi: Dict[str, Q] = None,
        temperature: Q = DEFAULT_T,
    ):
        self._pH = compartment_pH or {COMPARTMENT_ANY: DEFAULT_PH}
        self._pMg = compartment_pMg or {COMPARTMENT_ANY: DEFAULT_PMG}
        self._ionic_strength = compartment_I or {COMPARTMENT_ANY: DEFAULT_I}
        self._phi = compartment_phi or {COMPARTMENT_ANY: DEFAULT_PHI}
        self._temperature = temperature

    def pH(self, compartment: str) -> Q:
        """Gets the pH of a compartment."""
        pH = self._pH.get(compartment)
        if pH is None:
            pH = self._pH.get(COMPARTMENT_ANY)
        if pH is None:
            raise ValueError(f"Unspecified pH for compartment {compartment}")
        return pH

    def pMg(self, compartment: str) -> Q:
        """Gets the pMg of a compartment."""
        pMg = self._pMg.get(compartment)
        if pMg is None:
            pMg = self._pMg.get(COMPARTMENT_ANY)
        if pMg is None:
            raise ValueError(f"Unspecified pMg for compartment {compartment}")
        return pMg

    def ionic_strength(self, compartment: str) -> Q:
        """Gets the ionic strength of a compartment."""
        ionic_strength = self._ionic_strength.get(compartment)
        if ionic_strength is None:
            ionic_strength = self._ionic_strength.get(COMPARTMENT_ANY)
        if ionic_strength is None:
            raise ValueError(
                f"Unspecified ionic strength for compartment {compartment}"
            )
        return ionic_strength

    def phi(self, compartment: str) -> Q:
        """Gets the electrostatic potential of a compartment."""
        phi = self._phi.get(compartment)
        if phi is None:
            phi = self._phi.get(COMPARTMENT_ANY)
        if phi is None:
            raise ValueError(
                f"Unspecified electrostatic potential for compartment {compartment}"
            )
        return phi

    def T(self) -> Q:
        """Gets the temperature of the system."""
        return self._temperature

    @staticmethod
    def load(params_path: str | TextIO) -> "CompartmentParameters":
        """Loads the compartment parameters from a .csv file.

        Parameters
        ----------
        params_path : str | TextIO
            Name of a builtin parameter set (`default`, `e_coli`, or `human`),
            the path to a file containing the parameters, or a file-handle

        Returns
        -------
        CompartmentParameters
            New instance of this class, containing the parameters loaded from the file.
        """
        if isinstance(params_path, str):
            try:
                with (
                    resources.files(CompartmentParameters.RESOURCE_PACKAGE_NAME)
                    .joinpath(params_path + ".csv")
                    .open("rt") as fp
                ):
                    return CompartmentParameters.load(fp)
            except FileNotFoundError:
                params_path_ = get_path(params_path)
                if not params_path_.is_file() or not params_path_.exists():
                    raise FileNotFoundError(
                        f"Could not find parameters file {params_path_}"
                    )
                with open(params_path_, "rt") as fp:
                    return CompartmentParameters.load(fp)

        compartment_pH: Dict[str, Q] = {}
        compartment_pMg: Dict[str, Q] = {}
        compartment_I: Dict[str, Q] = {}
        compartment_phi: Dict[str, Q] = {}
        T: Q = None

        # Read compartment parameters from the file.
        params_df = pd.read_csv(params_path, comment="#")
        for _, row in params_df.iterrows():
            compartment = row["Compartment"]
            compartment_pH[compartment] = Q(row["pH"])
            compartment_pMg[compartment] = Q(row["pMg"])
            compartment_I[compartment] = Q(row["I"], "M")
            compartment_phi[compartment] = Q(row["phi"], "V")

            if T is None:
                T = Q(row["T"], "K")
            else:
                assert (
                    T is None or T.m == row.iloc[5]
                ), "All compartments must have the same temperature"

        return CompartmentParameters(
            compartment_pH, compartment_pMg, compartment_I, compartment_phi, T
        )
