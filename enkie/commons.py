"""Common definitions.
"""

import equilibrator_api


Q = equilibrator_api.Q_
"""Type used for describing quantities.
"""
