"""Metaclass for singleton classes."""


class SingletonMeta(type):
    """
    Metaclass for singleton classes. Credits:
    https://www.pythonprogramming.in/singleton-class-using-metaclass-in-python.html
    """

    def __init__(self, name, bases, dic):
        self.__single_instance = None
        super().__init__(name, bases, dic)

    def __call__(cls, *args, **kwargs):
        """Function call for the singleton class."""
        if cls.__single_instance:
            return cls.__single_instance
        single_obj = cls.__new__(cls)
        single_obj.__init__(*args, **kwargs)
        cls.__single_instance = single_obj
        return single_obj
