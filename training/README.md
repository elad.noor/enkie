# Training models for ENKIE

The models can be trained using the `train_models.R` script. This requires a few days of
computation time, please adjust the number of cores and threads to the capabilities of
your machine. Training the models requires the following R packages (please refer to the
respective documentations for installation instructions):
- [`rstan`](https://mc-stan.org/users/interfaces/rstan)
- [`cmdstanr`](https://mc-stan.org/cmdstanr/)
- [`brms`](https://paul-buerkner.github.io/brms/)