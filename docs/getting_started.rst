.. _getting_started:

Getting started
===============

************
Requirements
************

In order to install ENKIE you need:

* Windows, Linux or OSX operating system.
* Python 3.8 or newer.
* R installation (tested with R >= 4.2.0).
* ~3GB of free disk space.
  
************
Installation
************

First, install the R `brms` package (tested with version 2.17.6). In a R console, run:

.. code-block::

   install.packages("brms")

Once your system satisfies all the requirements, you can install ENKIE through the Python
Package Index:

.. code-block::

   pip install enkie

*Note*: at the first run, ENKIE will download the models and identifier mappings, this
may take a while.

*****
Usage
*****

Command Line Interface
----------------------

Once installed, ENKIE can be used from the command line with the :code:`enkie` command.
This approach is most suitable to estimate parameters for SBML models. Running

.. code-block::

   enkie --help

will show the available options. A complete example illustrating the usage on the
*e_coli_core* model can be found in the `git repository
<https://gitlab.com/csb.ethz/enkie/-/tree/main/examples/command_line/>`_ .


API
---

For more advanced applications and for finer control over the estimation process, ENKIE
can be accessed through a Python API. An simple example can be found in the `git
repository <https://gitlab.com/csb.ethz/enkie/-/tree/main/examples/API/>`_ . See the
complete API reference for additional information.