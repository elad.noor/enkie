ENKIE Overview
==============

This is the documentation for the ENKIE python package. ENKIE is available on `git
<https://gitlab.com/csb.ethz/enkie/>`_ and via `pip <https://pypi.org/project/enkie/>`_.
See :ref:`getting started <getting_started>` for the installation instructions and a
simple example. 

ENKIE is a python package for the prediction of kinetic parameter values and
uncertainties in metabolic networks. The package uses Bayesian Multilevel Models to
predict values and uncertainties from reaction, protein and metabolite identifiers. The
predictions are then combined with standard free energy estimates to ensure
thermodynamic consistency.

.. toctree::
   :hidden:
   :numbered:
   :maxdepth: 2
 
   self
   getting_started
   autoapi/index