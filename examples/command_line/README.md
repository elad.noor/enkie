## Example application of ENKIE to the *e_coli_core* model.

### Execution

In a terminal, move to this directory and run the command:
```
enkie -mn bigg.metabolite -rn bigg.reaction -cp e_coli e_coli_core.xml e_coli_core
```

### Output
ENKIE produces three files:
- `e_coli_core_mean.csv`: The predicted mean values of kinetic and thermodynamic parameters.
- `e_coli_core_cov.csv`: The covariance of the prediction uncertainty.
- `e_coli_core_genes.csv`: Metabolic models
  frequently contain reactions that can be catalyzed by multiple isozymes, which
  generally have different parameters. To address this, ENKIE creates one *rate law* for each isozyme. This file maps the ID of the created rate laws to the reaction and genes specified in the model.